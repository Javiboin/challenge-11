const chalk = require('chalk');
const express = require('express');
const app = express();

console.log(chalk.blue.bgRed('Hello World'));
const log = console.log;

function Persona(nombre,edad){
  this.name = nombre;
  this.age = edad;
};

Persona.prototype.updateEdad = function(nuevaEdad){
  this.age = nuevaEdad;
}

let Personas = [];
Personas.push(new Persona("Dave Grohl",52));
Personas.push(new Persona("Nate Mendel",52));
Personas.push(new Persona("Pat Smear",61));
Personas.push(new Persona("Taylor Hawkins",49));
Personas.push(new Persona("Chris Shiflett",50));
Personas.push(new Persona("Rami Jaffe",52));

function getDatos(){
  for (let i = 0; i < Personas.length; i++) {
    console.log(`Nombre: ${Personas[i].name}, edad: ${Personas[i].age}`);
  }};

function postDatos(){
  Personas.push(new Persona("Rami Jaffe",52));
};

function putDatos(){
  Personas[1].name = "Hola mundo";
  Personas[1].age = 33;
};

function delDatos(){
  Personas.pop();
}; 

app.get('/', function (req, res){
  res.send(getDatos());
});


app.post('/', function (req, res){
  res.send(postDatos());
});

app.put('/', function (req, res){
  res.send(putDatos());
});

app.delete('/', function (req, res){
  res.send(delDatos());
});

// prendemos el servidor
app.listen(3000, () =>{
  console.log('API REST corriendo en http://localhost:3000');
});